import { TestBed } from '@angular/core/testing';

import { SimulerTestService } from './simuler-test.service';

describe('SimulerTestService', () => {
  let service: SimulerTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimulerTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
